$(function() {
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        items: 1,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 5000,
        autoHeight: true
    })
    $('.signin').on('click', function(){
        if ($('.pp-forms-overlay').hasClass('hidden')) {
            $('.pp-forms-overlay').removeClass('hidden')  
        }
        if ($('#signin').hasClass('hidden')) {
            $('#signin').removeClass('hidden')  
        }
    })
    $('#agent').on('click', function(){
        if ($('.agent').hasClass('hidden')) {
            $('.agent').removeClass('hidden')
            $('.tourist').addClass('hidden')
        }
    })
    $('#tourist').on('click', function(){
        if ($('.tourist').hasClass('hidden')) {
            $('.tourist').removeClass('hidden')
            $('.agent').addClass('hidden')
        }
    })

    $('.addreview').on('click', function(){
        $(this).next('form').removeClass('hidden');
    })
    $('.addreview__close').on('click', function(){
        $(this).parent().parent().addClass('hidden');
    })

    getLocation();
    
});

function hiddenForm(formName) {
    $('.pp-forms-overlay').addClass('hidden')  
    $(formName).addClass('hidden')
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        console.log("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
    console.log(position.coords.latitude);
    console.log(position.coords.longitude);
}